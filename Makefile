all: dist/index.html

dist:
	mkdir -p dist/

dist/index.html: dist dist/converse.js-9.1.1 dist/video-js-7.19.2 dist/favicon.ico
	npx ejs src/index.ejs -f data.json -o dist/index.html

dist/favicon.ico: dist
	cp src/favicon.ico dist/

dist/converse.js-9.1.1: dist
	cp -r src/converse.js-9.1.1 dist/

dist/video-js-7.19.2: dist
	cp -r src/video-js-7.19.2 dist/

clean:
	rm -rf dist/

.PHONY: clean