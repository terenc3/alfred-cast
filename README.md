# Alfred Cast

> Basic Self-hosted stream server which uses [nginx](https://nginx.org/), [nginx-rtmp-module](https://github.com/arut/nginx-rtmp-module) and [converse.js](https://conversejs.org/)

## Demo

Visit https://live.roanapur.de and catch me live.

## Screenshots

// TODO

## Features

-   Easy installation on Debian Buster
-   Mostly relying on existing components
-   Multiple streams on the same application
-   Simple browser page for watching and chatting
-   External access to stream via url and chat via xmpp chat room
-   Global BetterTTV emotes

## Setup

1. HTTP and RTMP Server is **live.roanapur.de**.
2. XMPP Server is **chat.roanapur.de** and MUC is **conference.roanapur.de** and annonomous login is **anon.roanapur.de**.

### Nginx

#### Install nginx + rtmp module

Since Debian Buster part of the official packages

```
# apt install nginx-core libnginx-mod-rtmp
```

##### nginx.conf

Add a new block below _http_ with the following content

```
rtmp {
    server {
        listen 1935; # port number
        chunk_size 4096;

        # authentication will be handled later
        allow publish all;
        deny play all;

        # Enable to use $arg arguments in nginx blocks
        notify_method get;

        # application name
        application live {
            live on;
            record off;

            hls on;
            hls_path /tmp/hls/;
            hls_fragment 3;
            hls_playlist_length 60;

            # on_* hooks only work via http
            on_publish http://live.roanapur.de/on_publish;

            # notification via ntfy
            # exec_publish curl -X POST https://ntfy.home.roanapur.de/HerrTerenc3 -H "Icon: https://live.roanapur.de/favicon.ico" -H "Title: $name is live" -H "Priority: min" -H "Actions: view, View, https://live.roanapur.de/$name" -d "Join me!";
        }
   }
}
```

##### vhost.conf

```
server {
    listen 443 ssl;
    listen [::]:443 ssl;

    // setup your certifications properly

    server_name live.roanapur.de;
    sendfile off;
    tcp_nopush on;
    directio 512;

    # deployment location
    root /var/www/streamer;

    location / {
        try_files $uri uri/ /index.html;
    }

    # location of video snippets
    location /hls {
        types {
            application/vnd.apple.mpegurl m3u8;
            video/mp2t ts;
        }

        root /tmp/;

        # Prevent caching of HLS fragments
        add_header Cache-Control no-cache;

        # Allow web player to access our playlist
        add_header Access-Control-Allow-Origin *;
    }

    # proxy to bosh service
    location /http-bind/ {
        proxy_pass http://127.0.0.1:5280/http-bind/;
        proxy_buffering off;
        tcp_nodelay on;
    }
}

server {
    listen 80;
    listen [::]:80;

    server_name live.roanapur.de;

    # authentification, check app name, stream name and key
    location /on_publish {
        set $granted "";
        if ($arg_app = 'live') {
            set $granted APP;
        }
        if ($arg_name = '<STREAM_NAME>>') {
            set $granted "${granted}_NAME";
        }
        if ($arg_key = '<STREAM_KEY>') {
            set $granted "${granted}_KEY";
        }

        if ($granted = 'APP_NAME_KEY') {
            return 201;
        }
        return 403;
    }
}
```

### Prosody

Verify BOSH is enabled

```
modules_enabled = {
                "bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"
}

consider_bosh_secure = true;
```

Enable Multiuserchatrooms

#### prosody.conf

```
Component "conference.roanapur.de" "muc"
```

Provide anonymous host for login

#### vhost.cfg.lua

```
VirtualHost "anon.roanapur.de"
    authentication = "anonymous"
```

### Build public page

```
cp config/default.json data.json
make
```

### OBS

Setup Stream as Custom

-   Server: **rtmp://live.roanapur.de:1935/live**
-   Stream Key: **<STREAM_NAME>?key=<STREAM_KEY>**

## Todo

-   Offline image/video loop
-   Automatic refresh player
-   Support multiple streams better
-   Roles for Chat [XEP-0317: Hats](https://xmpp.org/extensions/xep-0317.html)
